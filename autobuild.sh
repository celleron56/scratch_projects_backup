cd ~/git
git clone https://github.com/SFML/SFML.git
cd SFML
git checkout 2.5.1
cd ~/git/SFML
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release -DBUILD_SHARED_LIBS=FALSE -DSFML_BUILD_WINDOW=FALSE -DSFML_BUILD_GRAPHICS=FALSE -DSFML_BUILD_DOC=FALSE -DSFML_BUILD_EXAMPLES=FALSE -DCMAKE_INSTALL_PREFIX=~/fs_libs/SFML ..
make install
